<?php

namespace App;

use DB;

class sExce extends \Exception{}

class Item{
	public static function safe($method=null, $args=null){
		try{
			switch($method){
				case 'add':
				case 'get':
				case 'set':
				case 'del':
				break;
				default:
					throw new sExce('invalid method '.$method);
			}
			return self::$method($args);
		}catch(sExce $e){
			return [
				'error'=>$e->getMessage(),
				'dbg'=>$args,
				];
		}catch(\Exception $e){
			return [
				'error'=>'Internal exception',
				'dbg'=>$e->getMessage(),
			];
		}
	}

	private static function getInput($need, $inp){
		$err = [];
		$ret = [];
		foreach($need as $n){
			if(!isset($inp[$n])){
				$err[]=$n;
			}else{
				$ret[$n]=$inp[$n];
			}
		}
		if(isset($err[0])){
			throw new sExce('missing params: '.implode(', ', $err));
		}
		return $ret;
	}

	private static function getItem($id){
		$r = DB::select('SELECT * FROM `items` WHERE `id`=?', [$id] );
		if(!isset($r[0])){
			return false;
		}
		return $r[0];
	}

	private static function getChild($id){
		return  DB::select('SELECT id, type, name FROM `items` WHERE `parent`=? ORDER BY `type` DESC, `name` ASC', [$id] );
	}

	public static function get($args){
		$id = intval($args['id']);
		if($id>0){
			$r = self::getItem($id);
			if($r === false){
				throw new sExce('object '.$id.' not found');
			}
		}else{
			return self::getRoot();
		}
		
		if($r->type ==="dir"){
			$r->childs = self::getChild($id);
		}
		return $r;
	}

	public static function getParentStat($id){
		$r =  DB::select('SELECT IFNULL(SUM(IF(`type`="file", 1,`files`)),0) as files, IFNULL(SUM(IF(`type`="dir", 1+`dirs`, 0)),0) as dirs, IFNULL(sum(`size`),0) as size FROM `items` WHERE `parent`=?', [$id]);
		
		return $r[0];
	}

	public static function getRoot(){
		$i = self::getParentStat(0);
		$i->name = 'Root';
		$i->id = 0;
		$i->childs = self::getChild(0);
		return $i;
	}

	public static function set($args){
		$id = intval($args['id']);
		$p = self::getInput(['name', 'text', 'size'], $args['post']);
		$item = self::getItem($id);
		$size = 0;
		DB::update('UPDATE `items` SET `name`=?, `text`=?, `size`=?, `updated_at`=CURRENT_TIMESTAMP() WHERE `id`=?', [$p['name'], $p['text'], $p['size'], $id]);
		self::updateParents($item->parent);
		return [
			'refresh'=>self::$path_to_root
		];
	}

	private static $path_to_root = [];

	private static function updateParents($id){
		self::$path_to_root[]=$id;
		if($id === 0){
			return false;
		}
		
		$item = self::getItem($id);
		$stat = self::getParentStat($id);
		DB::update('UPDATE `items` SET `files`=?, `dirs`=?, `size`=? WHERE `id`=?', [$stat->files, $stat->dirs, $stat->size, $id]);
		return self::updateParents($item->parent);
	}

	public static function add($args){
		$p = self::getInput(['type', 'parent'], $args['post']);
		if($p['type'] === 'dir'){
			$id = self::addDir($args);
		}else{
			$id = self::addFile($args);
		}
		self::updateParents(intval($p['parent']));
		return [
			 'item'=>self::getItem($id),
			 'refresh'=>self::$path_to_root
			];
	}

	public static function del($args){
		$item = self::getItem(intval($args['id']));
		if($item->type === 'dir'){
			$folders = [];
			$folders[]=$item->id;
			$next = true;
			while($next){
				$ids = implode(', ', $folders);
				$f = DB::select('SELECT `id` FROM `items` WHERE `parent` IN ('.$ids.') AND `id` NOT IN ('.$ids.')');
				if(!isset($f[0])){
					$next = false;
					continue;
				}
				foreach($f as $row){
					$folders[]=$row->id;
				}
			}
			$ids = implode(', ', $folders);
			DB::delete('DELETE FROM `items` WHERE `id` IN ('.$ids.') OR `parent` IN ('.$ids.')');
		}
		
		DB::delete('DELETE FROM `items` WHERE `id`=?', [$item->id]);
		self::updateParents($item->parent);
		return [
			'refresh'=>self::$path_to_root
		];
	}

	public static function addDir($args){
		$p = self::getInput(['parent', 'type', 'name'], $args['post']);
		$p['created_at']= \Carbon\Carbon::now();
		return DB::table('items')->insertGetId($p);
	}

	public static function addFile($args){
		$p = self::getInput(['name', 'type', 'parent', 'text', 'size'], $args['post']);
		$p['created_at']= \Carbon\Carbon::now();
		return DB::table('items')->insertGetId($p);
	}
}