<?php

use App\Item;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
	return redirect('/explorer.html');
});

$app->post('/add', function () use ($app) {
    return response()->json(Item::safe('add',['post'=>$_POST]));
});

$app->get('/get/{id}', function ($id) use ($app) {
    return response()->json(Item::safe('get',['id'=>$id]));
});

$app->post('/set/{id}', function ($id) use ($app) {
     return response()->json(Item::safe('set', ['id'=>$id, 'post'=>$_POST]));
});

$app->get('/del/{id}', function ($id) use ($app) {
    return response()->json(Item::safe('del', ['id'=>$id]));
});