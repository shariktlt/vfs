var Explorer = {
	selected_id:0,
	inited_root:false,
	store:{},
	init:function(){
		$('.item_0').on('click', function(event){
			this.onElementClick($(event.currentTarget));
		}.bind(this));
		this.editor =  $('.editor');
		this.details = $('.info > .details');
		this.getItem(0);
		this.editor.on('keydown', function(){
			this.updateSize();
		}.bind(this))
			.on('keydown', function(){
			this.updateSize();
		}.bind(this));
		$('.btn').on('click', function(event){
			this.onBtnClick($(event.currentTarget));
		}.bind(this));

	},
	onElementClick:function(el){
		console.log(el, 'click');
		
		if(this.selected_id !== el.attr('data-id')*1){
			this.selected_id = el.attr('data-id')*1;
			$('.selected').removeClass('selected');
			el.addClass('selected');
			el.removeClass('opened').addClass('opened');
			return this.getItem(this.selected_id);
		}
		if(el.hasClass('folder')){
			el.toggleClass('opened');
		}
	},
	onBtnClick:function(el){
		switch(el.attr('data-act')){
			case 'save':
				return this.saveFile();
			case 'del':
				return this.del();
			case 'create_dir':
				return this.create('dir');
			case 'create_file':
				return this.create('file');
		}
	},
	api:function(isPost, url, data, cb){
		if(typeof cb !== 'function'){
			cb = function(){};
		}
		jQuery.ajax({
			url: '/'+url,
			type: (isPost?'POST':'GET'),
			dataType: 'json',
			data: data,
			complete: function(xhr, textStatus) {
				//called when complete
			},
			success: function(data, textStatus, xhr) {
				//called when successful
				cb(true, data);
			},
			error: function(xhr, textStatus, errorThrown) {
				//called when there is an error
				alert('Error: '+textStatus);
				cb(false, textStatus);
			}
		});
		
	},
	getItem:function(id){
		id *= 1;
		var update = false;
		if(this.store.hasOwnProperty(id) ){
			if(this.store[id].update === undefined){
				$('.item_'+id).hasClass('notfilled');
				this.fillTree(this.store[id]);
				return this.showItem(this.store[id]);
			}
			update = true;
		}
		this.getItemApi(id, update);
	},
	getItemApi:function(id, update){
		this.api(false, 'get/'+id, null, function(ok, answ){
			if(!ok){
				return false;
			}
			if(answ.error !== undefined){
				return alert(answ.error);
			}
			this.onItemLoaded(answ, update);
			
		}.bind(this));
	},
	onItemLoaded:function(item, update){
		this.store[item.id*1]=item;
		
		console.log('loaded ', item);
		if(!update || this.selected_id === item.id*1){
			this.fillTree(item);
		}
		if(this.selected_id === item.id*1){
			this.showItem(item);
		}
		
	},
	fillTree:function(item){
		var h = $('.item_'+item.id);
		h.find('> .name').html(item.name);
		var l = h.find('> .folder-childs');
		l.find('> li').unbind().remove();
		$.each(item.childs,function(i,child){
			var row = $('<div class="item_'+child.id+'" data-id="'+child.id+'"></div>');
			row.on('click', function(event){
				event.stopPropagation();
				this.onElementClick($(event.currentTarget));
			}.bind(this));
			if(child.type === 'dir'){
				//dir
				row.addClass('folder').addClass('notfilled');
				var hov = $('<div class="hov">');
				hov.append($('<div class="isprite ico"></div>'))
					.append($('<div class="name">'+child.name+'</div>'));
				row.append(hov)
					.append('<ul class="folder-childs"></ul>');
			}else{
				row.addClass('file');
				row.append($('<div class="isprite ico"></div>'))
					.append($('<div class="name">'+child.name+'</div>'));
			}
			l.append($('<li></li>').append(row));
		}.bind(this));
	},
	showItem:function(item){
		this.currentItem = item;
		var cnt = $('.inspector');
		var editor = cnt.find('.editor');
		var buttons = cnt.find('.buttons');
		cnt.find('h1').html(item.name);
		cnt.find('.info > .btn').hide();
		if(item.id*1>0 ){
			cnt.find('.info > .del').show();
		}
		editor.hide();
		buttons.hide();
		cnt.find('h1').attr('contenteditable',false);
		if(item.type === 'file' ){
			cnt.find('.info > .save').show();
			cnt.find('h1').attr('contenteditable',true);
			editor.show();
			editor.html(item.text);
			this.updateSize();
		}else{
			cnt.find('.info .details').html(
				[
					this.formatSize(item.size),
					item.dirs+ ' '+this.declOfNum(item.dirs, ['папка', 'папки', 'папок']),
					item.files+' '+this.declOfNum(item.dirs, ['файл', 'файла', 'файлов']),
				].join(', ')
				);
			buttons.show();
		}
	},
	formatSize:function(n){
		n *=1;
		var i = 0, type = ['б','Кб','Мб','Гб','Тб','Пб'];
		while((n / 1000 | 0) && i < type.length - 1) {
			n /= 1024;
			i++;
		}
		return n.toFixed(2) + ' ' + type[i];
	},
	declOfNum:function(number, titles){
		number = Math.abs(number);
		cases = [2, 0, 1, 1, 1, 2];
		return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
	},
	updateSize:function(){
		var size = this.editor.html().length;
		this.details.html(this.formatSize(size));
		this.currentItem.size = size;
	},
	saveFile:function(){
		this.currentItem.text = this.editor.html();
		this.currentItem.size = this.currentItem.text.length;
		this.currentItem.name = $('.inspector h1').html();
		this.api(true, 'set/'+this.currentItem.id, this.currentItem, function(ok, answ){
			if(answ.error !== undefined){
				alert(answ.error);
			}else{
				$('.item_'+this.currentItem.id).find('> .name').html(this.currentItem.name);
				this.store[this.currentItem.id]=this.currentItem;
				if(answ.refresh){
					for (var i = answ.refresh.length - 1; i >= 0; i--) {
						this.store[answ.refresh[i]].update=true;
					}
				}
				alert('Сохранено');
			}
			
		}.bind(this));
	},
	create:function(type){
		var name = prompt('Введите название');
		if(name === null){
			return false;
		}
		this.api(true, 'add', {
			parent:this.selected_id,
			type: type,
			name: name,
			text: '',
			size: 0
		}, function(ok, answ){
			if(answ.error !== undefined){
				return alert(answ.error);
			}
			if(answ.refresh){
				for (var i = answ.refresh.length - 1; i >= 0; i--) {
					this.store[answ.refresh[i]].update=true;
				}
			}
			this.getItemApi(this.selected_id);
		}.bind(this));
	},
	del:function(){
		if(!confirm('Действительно удалить '+this.currentItem.name+'?')){
			return false;
		}
		this.api(false, 'del/'+this.selected_id, null, function(ok, answ){
			if(answ.error !== undefined){
				return alert(answ.error);
			}
			if(answ.refresh){
				for (var i = answ.refresh.length - 1; i >= 0; i--) {
					this.store[answ.refresh[i]].update=true;
				}
			}
			var p = this.currentItem.parent*1;
			this.getItemApi(p);
			$('.item_'+p).trigger('click');
		}.bind(this));
	}
};


jQuery(document).ready(function($) {
	Explorer.init();
});