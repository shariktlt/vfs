<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent')->unsigned();
            $table->enum('type', ['file', 'dir']);
            $table->string('name');
            $table->longText('text');
            $table->bigInteger('size')->unsigned();
            $table->integer('files')->unsigned();
            $table->integer('dirs')->unsigned();
            $table->timestamps();
            $table->index(['parent']);
            $table->index(['type']);
            $table->index(['type','name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('items');
    }
}
